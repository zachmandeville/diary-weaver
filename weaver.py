#!/usr/bin/env python3
import sys
import markdown
from string import Template
from datetime import datetime

# Establish Template

diaryTemplate = Template('''
    <div class="diary-entry" id="${date}">
      <div class="permalink-title">
	<a title="diary home" href="diary.html">	
	  <pre class="letterhead">
   +-+-+-+-+ +-+-+-+ +-+-+-+-+-+ +-+-+ 
   |F|r|o|m| |T|h|e| |D|i|a|r|y| |o|f| 
   +-+-+-+-+ +-+-+-+ +-+-+-+-+-+ +-+-+ 
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+     
   |C|o|o|l|g|u|y|.|W|e|b|s|i|t|e|  
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+  
   Zach Mandeville's digital home   
	</pre></a>
      </div>
      <h2><abbr title="${fulldate}">${title}</abbr></h2>
      <a class="share" title="shareable link" href="#${date}">Shareable Link</a>
      ${post}
    </div>
''')

# Take piped input and convert it to html from markdown
diaryText = markdown.markdown(sys.stdin.read())

# Create diary entry by substituting the post into the template
diaryEntry = diaryTemplate.substitute(
        title = sys.argv[1],
        date = datetime.now().strftime('%Y-%m-%d'),
        fulldate = datetime.now().strftime('%A the %dth of %B, %Y, %H:%M'),
        post = diaryText
        )

# Open up diary html and make its contents maniputalable

diary = open('/Users/Nelsonian/Projects/Web/coolguy/diary.html', 'r')
contents = diary.readlines()
diary.close() # Is maniputalable a word?

# Find where the diary entries actually begin, so you dont' write over the head tags.
def findStartingPoint(the_list, the_pattern):
    for ind, str in enumerate(the_list):
        if the_pattern in str:
            return ind + 1
    return -1

startingPoint = findStartingPoint(contents, '<!-- Begin Diary -->')

# Add the contents into the diary code, starting at the starting point.
contents.insert(startingPoint, diaryEntry)

# Write this changed diary content to the file.  Make it official.
diary = open('/Users/Nelsonian/Projects/Web/coolguy/diary.html', 'w')
contents = ''.join(contents)
diary.write(contents)
diary.close()

# you did it.




